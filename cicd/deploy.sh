#!/bin/bash
set -e # exit on error
source ../.env

# AWS REMOTE VARIABLES
# ------------------------
ROOT="APPS"
# ------------------------
# CI_COMMIT_BRANCH is an env variable
DESTINATION_FOLDER="$ROOT/$APP_NAME/$CI_COMMIT_BRANCH/"
SERVICE_NAME="$APP_NAME-$CI_COMMIT_BRANCH"

if [ -z "$CI_COMMIT_BRANCH" ]; then
  echo "'CI_COMMIT_BRANCH' environment variable is mandatory"
  exit 1
fi

mkdir -p $DESTINATION_FOLDER

main_function() {
  pm2 stop $SERVICE_NAME || true

  # mkdir $DESTINATION_FOLDER
  cd $DESTINATION_FOLDER
  CLONE="clone/"
  rimraf $CLONE
  git clone -b $CI_COMMIT_BRANCH $REPO_URL $CLONE

  cd $CLONE
  npm i # --omit=dev
  npm run build

  RUNTIME_LOGS='../runtime.log'
  touch $RUNTIME_LOGS
  export NODE_ENV="$([ "$CI_COMMIT_BRANCH" = "master" ] && echo "production" || echo "integration")"
  pm2 start build/index.js --name $SERVICE_NAME --log $RUNTIME_LOGS
  pm2 save
}

main_function >"${DESTINATION_FOLDER}cicd.log"
