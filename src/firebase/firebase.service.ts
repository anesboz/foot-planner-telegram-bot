import {
  DataSnapshot,
  Database,
  child,
  get,
  getDatabase,
  ref,
  set,
} from 'firebase/database'
import { ENV_METADATA } from '../common/shared/constants'
import { initializeApp } from 'firebase/app'
import { firebaseConfig } from './firebase.config'

class FirebaseService {
  db: Database
  ROOT_PATH: string

  constructor() {
    const app = initializeApp(firebaseConfig)
    this.db = getDatabase(app)
    this.ROOT_PATH =
      [ENV_METADATA.APP_NAME, ENV_METADATA.environment].join('/') + '/'
  }

  async get(appendedPath: string) {
    const path = this.ROOT_PATH + appendedPath
    let data: unknown = null
    try {
      const snapshot: DataSnapshot = await get(child(ref(this.db), path))
      data = snapshot.val()
    } catch (error) {
      console.log(error)
    }
    return data
  }

  update(appendedPath: string, data: unknown) {
    const path = this.ROOT_PATH + appendedPath
    const ref_ = ref(this.db, path)
    return set(ref_, data)
  }

  restore(appendedPath: string) {
    const path = this.ROOT_PATH + appendedPath
    const ref_ = ref(this.db, path)
    return set(ref_, null)
  }
}

export default new FirebaseService()
