import { MError } from './commands/domain/command.enums'
import loggerService from './common/helper/logger.service'
import { ENV_METADATA } from './common/shared/constants'
import botService from './services/bot/application/bot.service'

loggerService.line()
loggerService.start(`Bot`)
console.log(ENV_METADATA)

botService.bot.onText(/.*/, async (msg) => {
  if (botService.isNotCommand(msg)) {
    return
  }

  let responses: string[] = []
  try {
    responses = await botService.getResponses(msg)
  } catch (error) {
    if (!(error instanceof MError)) {
      console.log(`error:`)
      console.log(error)
    }
    responses = ['' + error]
  }
  const chatId = msg.chat.id
  for (const response of responses) {
    await botService.sendMessage(chatId, response)
  }
})
