import { Message } from 'node-telegram-bot-api'

export interface Parsed {
  command: string
  text: string
  message: Message
  json: object | null
  chatId: number
  isGroup: boolean
  isAdmin: boolean
}
