import TelegramBot from 'node-telegram-bot-api'
import { commands } from '../../../commands/application/commands'
import { Parsed } from '../domain/parser.interface'
import { validateMessage } from '../../../commands/domain/decorators/validateMessage.decorator'
import { ENV_METADATA } from '../../../common/shared/constants'
import utilsService from '../../utils/utils.service'
import { MError } from '../../../commands/domain/command.enums'
import htmlService from './html.service'

class BotService {
  bot: TelegramBot
  regex = /.*\/([A-Za-z]+)(.*)/

  constructor() {
    this.bot = new TelegramBot(ENV_METADATA.TOKEN, { polling: true })
  }

  @validateMessage()
  async getResponses(message: TelegramBot.Message): Promise<string[]> {
    const parsed = await this.parseMessage(message)
    const foundCommand = this.getFun(parsed.command)
    const responses = await foundCommand.fun(parsed)
    return responses
  }

  private getFun(command: string) {
    const foundCommand = commands.find(({ pattern }) => pattern === command)

    if (foundCommand == null) {
      throw new MError(
        `Je ne reconnais pas ${htmlService.codeInline(command)} 😳\n` +
          `Voir les commandes disponibles /help`
      )
    }

    return foundCommand
  }

  private async parseMessage(message: TelegramBot.Message): Promise<Parsed> {
    const text = message.text?.trim().replace(/\n/g, ' ') as string
    const command = text.replace(this.regex, '$1')
    const rest = text.trim().replace(this.regex, '$2') ?? '' // ?? '{}'

    let json: object | null = null
    let error = null
    // try to parse json
    try {
      json = JSON.parse(rest)
    } catch (err) {
      error = '' + err
    }

    const isGroup = ['group', 'supergroup'].includes(message.chat.type)
    const data = await this.bot?.getChatMember(
      message.chat.id,
      message.from?.id as number
    )
    const isAdmin = data?.status == 'creator' || data?.status == 'administrator'

    const chatId = message.chat.id

    const ret = {
      message,
      command,
      text: rest.trim(),
      json,
      chatId,
      isGroup,
      isAdmin,
    }
    return ret
  }

  isNotCommand(message: TelegramBot.Message) {
    return message.text == null || !this.regex.test(message.text)
  }

  async sendMessage(chatId: number, message: string) {
    await this.bot.sendMessage(chatId, message, { parse_mode: 'HTML' })
  }
}

export default new BotService()
