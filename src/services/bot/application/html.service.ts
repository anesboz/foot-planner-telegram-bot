class HtmlService {
  showHtmlExample(): string[] {
    const test = `
<b>bold</b>
<i>italic</i>
<u>underline</u>
<del>strikethrough</del>
<tg-spoiler>spoiler</tg-spoiler>
<a href="http://www.example.com/">inline URL</a>
<code>inline fixed-width code</code><pre><code class="language-ts">export default new GeneralCommands()</code></pre>
`
    return [test]
  }

  @escaping()
  bold(str: string) {
    return `<b>${str}</b>`
  }

  @escaping()
  italic(str: string) {
    return `<i>${str}</i>`
  }

  @escaping()
  underline(str: string) {
    return `<u>${str}</u>`
  }

  @escaping()
  strikethrough(str: string) {
    return `<del>${str}</del>`
  }

  @escaping()
  spoiler(str: string) {
    return `<tg-spoiler>${str}</tg-spoiler>`
  }

  @escaping()
  url(text: string, url: string) {
    return `<a href="${url}">${text}</a>`
  }

  @escaping()
  codeInline(code: string) {
    return `<code>${code}</code>`
  }

  @escaping()
  codeBlock(code: string, language = `json`) {
    return `<pre><code class="language-${language}">${code}</code></pre>
    `
  }
}

export default new HtmlService()

export function escaping(this: any) {
  return function (
    target: any,
    propertyKey: string,
    descriptor: PropertyDescriptor
  ) {
    const originalMethod = descriptor.value
    descriptor.value = function (...args: any[]) {
      const htmlEscapes: { [key: string]: string } = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#39;',
        '/': '&#x2F;',
      }

      const args2 = args.map((e, i) => {
        const newValue = e.replace(
          /[&<>"'/]/g,
          (match: string) => htmlEscapes[match]
        )
        return newValue
      })

      return originalMethod.apply(this, args2)
    }
    return descriptor
  }
}
