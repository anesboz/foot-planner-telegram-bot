import { MError } from '../../../../commands/domain/command.enums'

export enum WeekDays {
  LUNDI = 'Lundi',
  MARDI = 'Mardi',
  MERCREDI = 'Mercredi',
  JEUDI = 'Jeudi',
  VENDREDI = 'Vendredi',
  SAMEDI = 'Samedi',
  DIMANCHE = 'Dimanche',
}

export function getEnglishWeekName(frenchName: string) {
  const daysFrToEng = {
    [WeekDays.LUNDI]: 'Monday',
    [WeekDays.MARDI]: 'Tuesday',
    [WeekDays.MERCREDI]: 'Wednesday',
    [WeekDays.JEUDI]: 'Thursday',
    [WeekDays.VENDREDI]: 'Friday',
    [WeekDays.SAMEDI]: 'Saturday',
    [WeekDays.DIMANCHE]: 'Sunday',
  }
  const [french, english] =
    Object.entries(daysFrToEng).find(
      ([key, value]) => key.toLowerCase() === frenchName.toLowerCase()
    ) ?? []

  if (english == null) {
    return new MError(`Can not get english week-day for ${french}`)
  }
  return english
}
export interface Time {
  dayOfWeek: WeekDays
  H: number
  M: number
}

export function twoDigitsTime(time: Time): string {
  const HH = twoDigits(time.H)
  const MM = twoDigits(time.M)
  return `${HH}h${MM}`
}

export function twoDigits(num: number): string {
  return ('0' + num).slice(-2)
}
