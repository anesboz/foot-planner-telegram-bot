import TelegramBot from 'node-telegram-bot-api'

export interface Player extends TelegramBot.User {
  displayName: string
  confirmDeadline: string
}
