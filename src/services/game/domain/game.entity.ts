import { GameDTO } from '../application/game.dto'
import { Player } from './interfaces/player.interface'
import { Time } from './interfaces/time.interface'

export interface Game extends GameDTO {
  players?: Player[]
  isOpen: boolean
  inscriptions: Time
  kickOff: Time
}

export interface IRecord {
  games: Game[]
  metadata: string
  chatId: number
}

export const nutralRecord = {
  games: [],
  metadata: '',
}
