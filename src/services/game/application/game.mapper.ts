import { Game } from '../domain/game.entity'
import { GameDTO } from './game.dto'

export function toModel(dto: GameDTO): Game {
  return {
    ...dto,
    players: [],
    isOpen: false,
  }
}
