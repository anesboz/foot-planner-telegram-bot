import { Message } from 'node-telegram-bot-api'
import { Parsed } from '../../bot/domain/parser.interface'
import { MFun } from '../../../commands/domain/command.interface'
import botService from '../../bot/application/bot.service'

interface PartialParsed extends Partial<Parsed> {
  // command: string
  chatId: number
}

class AtomicService {
  async execRootCommand(fun: MFun, partialParsed: PartialParsed) {
    const parsed = this.createParsed(partialParsed)
    const responses = await fun(parsed)
    await botService.sendMessage(parsed.chatId, responses.join(' '))
  }

  private createParsed(partialParsed: PartialParsed) {
    const { chatId } = partialParsed

    const parsed: Parsed = {
      chatId,
      message: partialParsed.message ?? ({} as any),
      command: partialParsed.command ?? '',
      text: partialParsed.text ?? '',
      json: partialParsed.json ?? null,
      isGroup: partialParsed.isGroup ?? true,
      isAdmin: partialParsed.isAdmin ?? true,
    }
    return parsed
  }
}

export default new AtomicService()
