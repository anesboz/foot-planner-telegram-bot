import { Time } from '../domain/interfaces/time.interface'

export interface GameDTO {
  location: string
  inscriptions: Time
  kickOff: Time
  teamOf: number
  description?: string
}
