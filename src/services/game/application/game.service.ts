import nodeCron, { ScheduledTask } from 'node-cron'
import { Game, IRecord, nutralRecord } from '../domain/game.entity'
import { toModel } from './game.mapper'
import {
  validateIndex,
  validateSchema,
} from '../../../commands/domain/decorators/validateSchema.decorator'
import {
  gameSchema,
  getGameExample,
} from '../../../commands/domain/schemas/game.schemas'
import { Parsed } from '../../bot/domain/parser.interface'
import { GameDTO } from './game.dto'
import { groupChatOnly } from '../../../commands/domain/decorators/group.decorator'
import firebaseService from '../../../firebase/firebase.service'
import botService from '../../bot/application/bot.service'
import {
  getEnglishWeekName,
  twoDigits,
  twoDigitsTime,
} from '../domain/interfaces/time.interface'
import htmlService from '../../bot/application/html.service'
import utilsService from '../../utils/utils.service'
import { Player } from '../domain/interfaces/player.interface'
import { ENV_METADATA, Environment } from '../../../common/shared/constants'
import { MError } from '../../../commands/domain/command.enums'
import moment from 'moment'
import {
  toggleExample,
  toggleSchema,
} from '../../../commands/domain/schemas/toggle.schemas'
import atomicService from './atomic.service'

export const game_messages = {
  noGames: `Pas de matchs disponibles 💔`,
}

class GameService {
  cache: IRecord[] = []
  jobs: ScheduledTask[] = []

  constructor() {
    this.setAllCrons()
  }

  // ----------------- COMMAND
  @groupChatOnly(true)
  @validateSchema(gameSchema, getGameExample())
  async createGame(parsed: Parsed) {
    const { json, chatId } = parsed
    const game: Game = toModel(json as GameDTO)

    const record = await this.readRecord(chatId)
    record.games = [...(record.games ?? []), game]
    await this.updateRecord(chatId, record)
    this.setAllCrons()
    const allGamesMsg = await this.showGames(parsed)
    return [`Math bien créé ✔️`, ...allGamesMsg]
  }

  @validateIndex()
  @groupChatOnly(true)
  async deleteGame(parsed: Parsed) {
    const { chatId } = parsed
    const record = await this.readRecord(chatId)
    const games = record.games ?? []
    if (games.length === 0) {
      return [game_messages.noGames]
    }
    const { index } = this.getGameIndex(parsed, games.length)
    record.games = utilsService.delete<Game>(games, index)
    await this.updateRecord(chatId, record)
    this.setAllCrons()
    const allGamesMsg = await this.showGames(parsed)
    return [`Math supprimé 🗑️`, ...allGamesMsg]
  }

  @validateIndex()
  @validateSchema(toggleSchema, toggleExample)
  @groupChatOnly(true)
  async toggleInscriptions(parsed: Parsed) {
    const { chatId, json } = parsed
    const record = await this.readRecord(chatId)
    const games = record.games ?? []
    if (games.length === 0) {
      return [game_messages.noGames]
    }

    const { open, reset } = json as any
    const { index, indexToPrint } = this.getGameIndex(parsed, games.length)
    const game = games[index]

    game.isOpen = open

    const prefix =
      index == 0 ? '' : htmlService.bold(`Match "${indexToPrint}": `)

    const noMatch = index == 0 ? '' : indexToPrint
    let msg = prefix + `Inscriptions fermées 🔒`
    if (game.isOpen) {
      msg =
        prefix +
        `Inscriptions ouvertes 🏁\nUtilisez /addMe${noMatch} pour s'inscrire.`
      game.players = []
    }
    if (reset === true) {
      game.players = []
    }

    await this.updateRecord(chatId, record)

    // if (forceState) {
    // }
    return [msg]
  }

  @groupChatOnly()
  async showGames(parsed: Parsed) {
    const { chatId } = parsed
    const record = await this.readRecord(chatId)
    const games = record.games ?? []
    let str = game_messages.noGames
    if (games.length > 0) {
      str = toString(games, this).join('')
    }
    return [htmlService.underline('⚽ Liste des matchs:\n') + str]

    function toString(games: Game[], context: any) {
      return games.map((game, i) => {
        const { kickOff, isOpen, inscriptions, teamOf, description, location } =
          game
        const { dayOfWeek, H, M } = kickOff
        const time = `${dayOfWeek} ${twoDigitsTime(kickOff)}`
        const players = game.players ?? []
        const InscriptionsTxt = htmlService.bold(
          isOpen ? 'ouvertes' : 'fermées'
        )
        const additionalMsg = isOpen
          ? `ferme ${dayOfWeek} à ${twoDigits(H - 1)}h${twoDigits(M)}`
          : `ouvre ${inscriptions.dayOfWeek} à ${twoDigitsTime(inscriptions)}`

        const { titulaires, remplacants } = context.getTitulaireNRemplacant(
          players,
          teamOf
        )
        const ret = `
│─ ${htmlService.bold(`Match "${i + 1}"`)}
│ ├⏲️ ${time}
│ ├📍 ${location}
│ ├📢 ${description}
│ ├🏃‍♂️ Titul. ${titulaires.length}/${2 * teamOf} | Remp. ${remplacants.length}
│ └👉 Inscriptions: ${InscriptionsTxt} 
│     (${htmlService.italic(additionalMsg)})
`
        return ret
      })
    }
  }

  @groupChatOnly(true)
  async dbRestore(parsed: Parsed) {
    const { chatId } = parsed
    await firebaseService.restore('' + chatId)
    this.setAllCrons()
    return [`DB restored for chat: ${chatId}`]
  }

  // ----------------- PRIVATE
  async readRecord(chatId: number) {
    let record = this.cache.find((e) => e.chatId === chatId)
    if (record != null) {
      return record
    }
    const candidat = (await firebaseService.get(chatId + '/')) as IRecord | null
    record = candidat ?? {
      ...nutralRecord,
      chatId,
    }
    this.updateRecord(chatId, record)
    return record
  }

  async updateRecord(chatId: number, record: IRecord) {
    let index = this.cache.findIndex((e) => e.chatId === chatId)
    index = index > 0 ? index : this.cache.length
    this.cache.splice(index, 1, record)
    await firebaseService.update('' + chatId, record)
  }

  // @groupChatOnly()
  private async setAllCrons(): Promise<void> {
    // stop all previous jobs
    this.jobs.forEach((job) => job.stop())

    // get all games for all chats
    const gamesNchatId = await this.getAllGames()

    console.log('Jobs :')
    // for eatch chat set
    gamesNchatId.map((gameN, i) => {
      const { inscriptions, kickOff, chatId } = gameN
      const times = [{ ...inscriptions }, { ...kickOff, H: kickOff.H - 1 }]
      // affichage
      const toPrint = `Chat: ${chatId}, Game ${i}: insc. ${twoDigitsTime(
        inscriptions
      )} kickof. ${twoDigitsTime(kickOff)}`
      console.log(toPrint)

      times.map((time, iTime) => {
        const { H, M, dayOfWeek } = time
        let englishDay = getEnglishWeekName(dayOfWeek)
        englishDay = '*' // DEV
        const job = nodeCron.schedule(
          `${M} ${H} * * ${englishDay}`,
          async () => {
            try {
              const value = (iTime + 1) % 2
              atomicService.execRootCommand(
                this.toggleInscriptions.bind(this),
                {
                  chatId,
                  json: { open: value, reset: value },
                }
              )
            } catch (error) {
              console.log(`error:`, error)
            }
          },
          {
            scheduled: true,
            timezone: 'Europe/Paris',
          }
        )
        this.jobs.push(job)
      })
    })
  }

  async getAllGames() {
    const records = (await firebaseService.get('')) as IRecord[]
    const values = Object.values(records)
    const gamesNchatId = values.flatMap(
      ({ games, chatId }) =>
        games?.map((game) => ({
          ...game,
          chatId,
        })) ?? []
    )
    return gamesNchatId
  }

  getGameIndex(parsed: Parsed, max: number) {
    const { text } = parsed
    const indexToPrint = parseInt(text)
    const index = indexToPrint - 1
    if (index >= max) {
      throw new MError(
        `Le match "${indexToPrint}" n'existe pas !\nUtilisez /games pour voir les matches disponibles.`
      )
    }
    return { index, indexToPrint }
  }

  getTitulaireNRemplacant(players: Player[], teamOf: number) {
    const titulaires = players.slice(0, 2 * teamOf)
    const remplacants = players.slice(2 * teamOf)
    return { titulaires, remplacants }
  }
}

export default new GameService()
