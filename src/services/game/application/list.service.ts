import { validateIndex } from '../../../commands/domain/decorators/validateSchema.decorator'
import { Parsed } from '../../bot/domain/parser.interface'
import { groupChatOnly } from '../../../commands/domain/decorators/group.decorator'
import { twoDigits, twoDigitsTime } from '../domain/interfaces/time.interface'
import htmlService from '../../bot/application/html.service'
import utilsService from '../../utils/utils.service'
import { Player } from '../domain/interfaces/player.interface'
import { MError } from '../../../commands/domain/command.enums'
import moment from 'moment'
import gameService, { game_messages } from './game.service'
import { IRecord } from '../domain/game.entity'
import { ENV_METADATA, Environment } from '../../../common/shared/constants'
import atomicService from './atomic.service'
import { Message } from 'node-telegram-bot-api'
import botService from '../../bot/application/bot.service'

enum ListAction {
  ADD_ME = 'ADD_ME',
  CANCEL_ME = 'CANCEL_ME',
  CONFIRM = 'CONFIRM',
}

class ListService {
  @validateIndex()
  @groupChatOnly()
  async showList(parsed: Parsed) {
    const { chatId, message } = parsed
    const record = await this.readRecord(chatId)
    const games = record.games ?? []
    if (games.length === 0) {
      return [game_messages.noGames]
    }
    const { index, indexToPrint } = gameService.getGameIndex(
      parsed,
      games.length
    )
    const game = games[index]

    const { teamOf } = game
    game.players = game.players ?? []

    const { titulaires, remplacants } = gameService.getTitulaireNRemplacant(
      game.players,
      teamOf
    )

    const toStr = (list: Player[]) =>
      list
        .map((player, i) => {
          const suffix =
            player.confirmDeadline?.length > 0
              ? htmlService.italic(`(à confirmer ⌛)`)
              : ''
          return `├ ${twoDigits(i + 1)}- ${player.displayName} ${suffix}`
        })
        .join('\n')

    const tMessage =
      `├─ ⚔️ ${htmlService.bold('Titulaires')}\n` + toStr(titulaires)

    const rMessage =
      remplacants.length > 0
        ? `├─ 💀 ${htmlService.bold('Remplaçants')}\n` + toStr(remplacants)
        : ``

    const { dayOfWeek, H, M } = game.kickOff
    const time = `${dayOfWeek} ${H}h${M}`
    const enTete = `⏲️ ${time}\n📍 ${game.location}`

    return [`${enTete}\n${tMessage}\n${rMessage}`]
  }

  addMe(parsed: Parsed) {
    return this.listAction(parsed, ListAction.ADD_ME)
  }

  cancelMe(parsed: Parsed) {
    return this.listAction(parsed, ListAction.CANCEL_ME)
  }

  confirm(parsed: Parsed) {
    return this.listAction(parsed, ListAction.CONFIRM)
  }

  @validateIndex()
  @groupChatOnly()
  private async listAction(parsed: Parsed, type: ListAction) {
    const { chatId, message } = parsed
    const record = await this.readRecord(chatId)
    const games = record.games ?? []
    if (games.length === 0) {
      return [game_messages.noGames]
    }
    const playerDTO = message.from as Player
    const displayName = playerDTO.username ?? playerDTO.first_name
    const { index, indexToPrint } = gameService.getGameIndex(
      parsed,
      games.length
    )
    const game = games[index]
    const players = game.players ?? []
    const player = players.find((e) => e.id === playerDTO.id)
    const indexPlayer = players.findIndex((e) => e.id === playerDTO.id)

    const { dayOfWeek } = game.inscriptions
    const prefix =
      index == 0 ? '' : htmlService.bold(`Match "${indexToPrint}": `)
    if (!game.isOpen) {
      const time = twoDigitsTime(game.inscriptions)
      throw new MError(
        prefix + `Inscription fermée ⛔.\nOuvre ${dayOfWeek} à ${time}`
      )
    }
    let response = ''
    // ----------------
    switch (type) {
      case ListAction.ADD_ME: {
        if (player != null && !ENV_METADATA.isDev) {
          throw new MError(`@${displayName} tu es déjà dans la liste 🖕`)
        }
        const newPlayer = { ...playerDTO, displayName }
        game.players = [...(game.players ?? []), newPlayer]
        response = `@${displayName} a été ajouté dans la liste 🎉`
        break
      }
      case ListAction.CANCEL_ME: {
        if (player == null) {
          throw new MError(`@${displayName} tu n'es meme pas dans la liste 💩`)
        }
        const candidat = players[game.teamOf * 2]
        game.players = utilsService.delete<Player>(players, indexPlayer)
        response = prefix + `${displayName} a annulé ! 🟥\n`
        const deadline = moment().add(60, 'minutes')
        if (candidat != null) {
          response += `@${candidat.displayName} à ton tour 😃. Tu as 1 heure pour confirmer ta présence /confirm`
          candidat.confirmDeadline = deadline.format()

          const VERIFY_DELAI_MIN = 0.1
          setTimeout(async () => {
            console.log('verifying')
            const message = { from: candidat } as unknown as Message
            console.log(`message:`, message)
            if (candidat.confirmDeadline.length > 0) {
              atomicService.execRootCommand(this.cancelMe.bind(this), {
                chatId,
                message,
              })
            }
          }, VERIFY_DELAI_MIN * 60 * 100)
        }

        break
      }
      case ListAction.CONFIRM: {
        if (player == null) {
          throw new MError(`@${displayName} tu n'es meme pas dans la liste 💩.`)
        }
        if (!(player.confirmDeadline?.length > 0)) {
          throw new MError(
            `@${displayName} tu n'es pas en attente de confirmation ❌.`
          )
        }
        player.confirmDeadline = ''
        response = `@${displayName} a confirmé et est titulaire maintenant 🎉.`
        break
      }

      default:
        break
    }

    // ----------------
    await this.updateRecord(chatId, record)
    return [response]
  }

  private readRecord(chatId: number) {
    return gameService.readRecord.call(gameService, chatId)
  }
  private updateRecord(chatId: number, record: IRecord) {
    return gameService.updateRecord.call(gameService, chatId, record)
  }
}

export default new ListService()
