class UtilsService {
  toLeft<Type>(list: Array<Type>, index: number) {
    const elem = list[index]
    // delete in current position
    list.splice(index, 1)
    list.splice(index - 1, 0, elem)
    return list
  }

  toRight<Type>(list: Array<Type>, index: number) {
    const elem = list[index]
    // delete in current position
    list.splice(index, 1)
    list.splice(index + 1, 0, elem)
    return list
  }

  delete<Type>(list: Array<Type>, index: number) {
    if (list.length > 0) list.splice(index, 1)
    return list
  }

  getFunctionName(fun: any): string {
    const names = fun.name.split(' ')
    console.log(`names:`, names)
    return names[names.length - 1]
  }

  cap(string: string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
  }
}

export default new UtilsService()
