import { Environment } from './constants'

export function getBotConfig(APP_NAME: string) {
  const TOKENS = {
    DEV: `6263722714:AAHOd24-3f6UBqSlaPVAofnVGQ4KniqKarc`,
    PREPROD: `6029032801:AAHLhUKiKLENsvhHSO4byyIJDgvYkkMCRDY`,
    PROD: `6087858025:AAGfeWZX-SepPdSN4eUsMJ1IimR14MdCGbs`,
  }

  const environment = process.env.NODE_ENV ?? Environment.DEVELOPMENT
  let TOKEN = TOKENS.DEV
  if (environment === Environment.INTEGRATION) {
    TOKEN = TOKENS.PREPROD
  }
  if (environment === Environment.PRODUCTION) {
    TOKEN = TOKENS.PROD
  }

  return {
    APP_NAME,
    environment,
    TOKEN,
    isDev: environment === Environment.DEVELOPMENT,
  }
}
