import { getBotConfig } from "./botConfig"

const APP_NAME = "FPB Foot Planner Bot"

export enum Environment {
  DEVELOPMENT = "development",
  INTEGRATION = "integration",
  PRODUCTION = "production",
}

export const ENV_METADATA = getBotConfig(APP_NAME)
