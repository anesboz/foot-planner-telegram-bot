import moment from 'moment'
import chalk from 'chalk'

const LARGE = 5
class LoggerService {
  private colorIndex = 0
  private readonly colors = [
    chalk.yellowBright,
    chalk.redBright,
    chalk.cyanBright,
  ]

  start(message?: string) {
    this.generic(message)
    ++this.colorIndex
  }

  end(message?: string) {
    --this.colorIndex
    this.generic(message, true)
  }

  private generic(message = ` `, isEnd = false) {
    const traits = '═'.repeat(LARGE)
    const uniqColor = this.colors[this.colorIndex % this.colors.length]
    const array = [
      uniqColor((isEnd ? `╚` : `╔`) + traits),
      this.timestamp(),
      chalk.underline(
        isEnd ? chalk.redBright(` END `) : chalk.greenBright(`START`)
      ),
      chalk.reset(message),
      chalk.reset(` `),
      // traits + (isEnd ? `╝` : `╗`),
    ]
    console.log(array.join(' '))
  }

  private timestamp() {
    const DATE_FORM = 'YYYY-MM-DD HH:mm:ss'
    return '[' + moment.utc().format(DATE_FORM) + ']'
  }

  info(message: string) {
    console.log(chalk.yellowBright(this.timestamp()), message)
  }

  success(message: string) {
    const array = [
      this.timestamp(),
      chalk.greenBright(chalk.underline(`_OK_`)),
      chalk.greenBright(message),
    ]
    console.log(array.join(' '))
  }
  error(message: string) {
    const array = [
      this.timestamp(),
      chalk.redBright(chalk.underline(`_ERROR_`)),
      chalk.redBright(message),
    ]
    console.log(array.join(' '))
  }

  line() {
    const traits = '═'.repeat(50)
    console.log(`\n${chalk.yellowBright(traits)}\n`)
  }
}

export default new LoggerService()
