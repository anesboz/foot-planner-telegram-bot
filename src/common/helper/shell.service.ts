import { spawn } from 'child_process'
import fs from 'fs'

class ShellService {
  async run(command: string, quiet = false): Promise<string> {
    const args = ['-Command', `& { ${command} }`]
    const childProcess = spawn('powershell.exe', args, { stdio: 'pipe' })

    return new Promise((resolve, reject) => {
      const chunks: Uint8Array[] = []

      childProcess.stdout.on('data', (chunk: Uint8Array) => {
        chunks.push(chunk)
      })

      childProcess.stderr.on('data', (chunk: Uint8Array) => {
        chunks.push(chunk)
      })

      childProcess.on('close', (code: number) => {
        const output = Buffer.concat(chunks).toString()

        if (code === 0) {
          resolve(output)
          if (!quiet) {
            console.log(output)
          }
        } else {
          reject(
            new Error(
              `Command "${command}" failed with code ${code}: ${output}`
            )
          )
        }
      })

      childProcess.on('error', (error: Error) => {
        reject(error)
      })
    })
  }

  createFolderIfNotExist(absolutePath: string) {
    if (!fs.existsSync(absolutePath)) {
      fs.mkdirSync(absolutePath, { recursive: true })
    }
  }
}

export default new ShellService()
