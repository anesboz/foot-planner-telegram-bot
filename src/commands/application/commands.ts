import { Command } from '../domain/command.interface'
import { Constraints } from '../domain/command.enums'
import _ from 'lodash'
import gameService from '../../services/game/application/game.service'
import htmlService from '../../services/bot/application/html.service'
import listService from '../../services/game/application/list.service'

export const commands: Command[] = [
  // general
  {
    pattern: 'hello',
    fun: () => [`/hello darkness my old friend 🎵`],
    constraint: null,
    description: `Saying hello.`,
  },
  {
    pattern: 'help',
    fun: help,
    constraint: Constraints.GROUP_ONLY,
    description: `Afficher toutes les commandes disponibles.`,
  },
  {
    pattern: 'showHtmlExample',
    fun: htmlService.showHtmlExample.bind(htmlService),
    constraint: null,
    description: `testMD`,
  },
  // // gameCommand
  // LIST
  {
    pattern: 'showList',
    fun: listService.showList.bind(listService),
    constraint: Constraints.GROUP_ONLY,
    description: `list`,
  },
  {
    pattern: 'addMe',
    fun: listService.addMe.bind(listService),
    constraint: Constraints.GROUP_ONLY,
    description: `addMe`,
  },
  {
    pattern: 'cancelMe',
    fun: listService.cancelMe.bind(listService),
    constraint: Constraints.GROUP_ONLY,
    description: `cancelMe`,
  },
  {
    pattern: 'confirm',
    fun: listService.confirm.bind(listService),
    constraint: Constraints.GROUP_ONLY,
    description: `confirm`,
  },
  // GAMMMMESSSS
  // {
  //   pattern: 'setCrons',
  //   fun: gameService.setCrons.bind(gameService),
  //   constraint: Constraints.GROUP_ONLY_ADMIN,
  //   description: `setCrons`,
  // },
  {
    pattern: 'toggleInscriptions',
    fun: gameService.toggleInscriptions.bind(gameService),
    constraint: Constraints.GROUP_ONLY_ADMIN,
    description: `toggleInscriptions`,
  },
  {
    pattern: 'showGames',
    fun: gameService.showGames.bind(gameService),
    constraint: Constraints.GROUP_ONLY,
    description: `Afficher les matches disponibles.`,
  },
  {
    pattern: 'createGame',
    fun: gameService.createGame.bind(gameService),
    constraint: Constraints.GROUP_ONLY_ADMIN,
    description: `Ajouter un nouveau match.`,
  },
  {
    pattern: 'deleteGame',
    fun: gameService.deleteGame.bind(gameService),
    constraint: Constraints.GROUP_ONLY_ADMIN,
    description: `deleteGame`,
  },
  {
    pattern: 'dbRestore',
    fun: gameService.dbRestore.bind(gameService),
    constraint: Constraints.GROUP_ONLY_ADMIN,
    description: `Restore DB`,
  },
  // // {
  // pattern://    'deleteGame'
  // fun: gameCommands.deleteGame.bind(gameCommands),
  // //   constraint: Privileges.ADMIN,
  // //   description: `Supprimer un match.`,
  // // },
  // // {
  // pattern://    'addMe'
  // fun: gameCommands.addMe.bind(gameCommands),
  // //   constraint: Privileges.PUBLIC,
  // //   description: `Ajouter à un match.`,
  // // },
  // // {
  // pattern://    'list'
  // fun: gameCommands.list.bind(gameCommands),
  // //   constraint: Privileges.PUBLIC,
  // //   description: `List d'un match`,
  // // },
  // // {
  // pattern://    'startInscriptions'
  // fun: gameCommands.startInscriptions.bind(gameCommands),
  // //   constraint: Privileges.ADMIN,
  // //   description: `startInscriptions`,
  // // },
  // // {
  // pattern://    'closeInscriptions'
  // fun: gameCommands.closeInscriptions.bind(gameCommands),
  // //   constraint: Privileges.ADMIN,
  // //   description: `closeInscriptions`,
  // // },
  // // {
  // pattern://    'dbRestore'
  // fun: gameCommands.dbRestore.bind(gameCommands),
  // //   constraint: Privileges.ADMIN,
  // //   description: `dbRestore`,
  // // },
]

function help(): string[] {
  const grouped = _.groupBy(commands, 'constraint')
  const retValues = [`🤖 Commandes disponibles:`]
  _.forEach(grouped, (commands, constraint) => {
    const childrend = commands.map(
      (command) =>
        `/${command.pattern} : ${htmlService.italic(command.description)}`
    )
    constraint = constraint == 'null' ? 'PUBLIC' : constraint
    const constraintTxt = htmlService.bold(constraint)
    retValues.push(`\n- ${constraintTxt}\n${childrend.join('\n')}`)
  })

  return [retValues.join('\n')]
}
