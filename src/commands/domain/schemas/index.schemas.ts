import Joi from 'joi'

export const indexSchema = Joi.object({
  index: Joi.number().required(),
})

export const indexSample = { index: 0 }
