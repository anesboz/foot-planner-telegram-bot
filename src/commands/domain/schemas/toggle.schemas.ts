import Joi from 'joi'

export const toggleSchema = Joi.object({
  open: Joi.number()
    .valid(...[0, 1])
    .required(),
  reset: Joi.number()
    .valid(...[0, 1])
    .required(),
})

export const toggleExample = {
  open: 1,
  reset: 0,
}
