import Joi from 'joi'
import { WeekDays } from '../../../services/game/domain/interfaces/time.interface'
import moment from 'moment'
import { ENV_METADATA, Environment } from '../../../common/shared/constants'

const timeSchema = Joi.object().keys({
  dayOfWeek: Joi.string()
    .valid(...Object.values(WeekDays))
    .insensitive()
    .required(),
  H: Joi.number().min(0).max(23).required(),
  M: Joi.number().min(0).max(59).required(),
})

export const gameSchema = Joi.object({
  location: Joi.string().required(),
  inscriptions: timeSchema,
  kickOff: timeSchema,
  teamOf: Joi.number().required(),
  description: Joi.string(),
})

const gameExample = {
  location: 'Porte de clignoncourt',
  inscriptions: {
    dayOfWeek: WeekDays.LUNDI,
    H: 12,
    M: 0,
  },
  kickOff: {
    dayOfWeek: WeekDays.VENDREDI,
    H: 18,
    M: 30,
  },
  teamOf: 9,
  description: `Pas de retard svp`,
}

export function getGameExample() {
  if (!ENV_METADATA.isDev) {
    return gameExample
  }
  const copy = { ...gameExample }
  const insc = moment().add(2, 'minutes')
  const kick = moment().add(3, 'minutes')

  copy.inscriptions.dayOfWeek = insc.locale('fr').format('dddd') as WeekDays
  copy.inscriptions.H = insc.hours()
  copy.inscriptions.M = insc.minutes()

  copy.kickOff.dayOfWeek = kick.locale('fr').format('dddd') as WeekDays
  copy.kickOff.H = kick.hours() + 1
  copy.kickOff.M = kick.minutes()

  return copy
}
