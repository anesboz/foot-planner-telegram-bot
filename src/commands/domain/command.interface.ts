import { Parsed } from '../../services/bot/domain/parser.interface'
import { Constraints } from './command.enums'

export type MFun = (parsed: Parsed) => string[] | Promise<string[]>

export interface Command {
  pattern: string
  fun: MFun
  constraint: Constraints | null
  description: string
}
