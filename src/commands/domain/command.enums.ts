export enum Constraints {
  GROUP_ONLY_ADMIN = 'GROUP_ONLY_ADMIN',
  GROUP_ONLY = 'GROUP_ONLY',
  PRIVATE_ONLY = 'PRIVATE_ONLY',
}

export class MError extends Error {
  constructor(message: string) {
    super(message)
    this.name = 'MError'
  }

  toString() {
    return this.message
  }
}
