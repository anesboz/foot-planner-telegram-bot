import Joi from 'joi'
import htmlService from '../../../services/bot/application/html.service'
import { Parsed } from '../../../services/bot/domain/parser.interface'
import { MError } from '../command.enums'

export function validateSchema(this: any, schema: Joi.Schema, example?: any) {
  return function (
    target: any,
    propertyKey: string,
    descriptor: PropertyDescriptor
  ) {
    const originalMethod = descriptor.value
    descriptor.value = function (...args: any[]) {
      const parsed = args[0] as Parsed

      const { command, text, json, message } = parsed

      const { error } = schema.validate(json)
      const errorMsg = '' + error
      if (error != null) {
        let toThrow = `Le json attendu est mal formaté ❌\n${htmlService.codeInline(
          errorMsg
        )}`

        const codeTxt = htmlService.codeBlock(
          `/${command}\n${JSON.stringify(example, null, 2)}`
        )

        if (example != null) {
          toThrow += `\n\nVoici un exemple à suivre: 👇${codeTxt}`
        }
        throw new MError(toThrow)
      }

      return originalMethod.apply(this, args)
    }
    return descriptor
  }
}

export function validateIndex(this: any, example = `/command 1`) {
  return function (
    target: any,
    propertyKey: string,
    descriptor: PropertyDescriptor
  ) {
    const originalMethod = descriptor.value
    descriptor.value = function (...args: any[]) {
      const parsed = args[0] as Parsed

      let text = parsed.text
      parsed.text = text?.length === 0 ? '1' : text
      text = parsed.text

      let num = 1

      try {
        num = parseInt(text)
      } catch (err) {
        console.log(`err:`, err)
      }

      const { error } = Joi.object({
        num: Joi.number().min(1).required(),
      }).validate({
        num,
      })

      const errorMsg = '' + error
      if (error != null) {
        let toThrow = `Index mal formaté ❌\n${htmlService.codeInline(
          errorMsg
        )}`

        if (example != null) {
          toThrow += `\n\nVoici un exemple à suivre: 👇${htmlService.codeInline(
            example
          )}`
        }
        throw new MError(toThrow)
      }

      return originalMethod.apply(this, args)
    }
    return descriptor
  }
}
