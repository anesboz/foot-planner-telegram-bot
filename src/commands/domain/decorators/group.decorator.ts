import htmlService from '../../../services/bot/application/html.service'
import { Parsed } from '../../../services/bot/domain/parser.interface'
import { Constraints, MError } from '../command.enums'

export function groupChatOnly(this: any, mustBeAnAdmin = false) {
  return function (
    target: any,
    propertyKey: string,
    descriptor: PropertyDescriptor
  ) {
    const originalMethod = descriptor.value
    descriptor.value = function (...args: any[]) {
      const parsed = args[0] as Parsed
      const { isGroup, isAdmin } = parsed
      if (!isGroup) {
        throw new MError(
          `This is a ${htmlService.bold(Constraints.GROUP_ONLY)} command.`
        )
      }
      if (mustBeAnAdmin && !isAdmin) {
        throw new MError(
          `This is a ${htmlService.bold(Constraints.GROUP_ONLY_ADMIN)} command.`
        )
      }
      return originalMethod.apply(this, args)
    }
    return descriptor
  }
}
export function adminOnly(this: any) {
  return function (
    target: any,
    propertyKey: string,
    descriptor: PropertyDescriptor
  ) {
    const originalMethod = descriptor.value
    descriptor.value = function (...args: any[]) {
      const parsed = args[0] as Parsed

      const { isGroup } = parsed

      if (!isGroup) {
        throw new MError(
          `This is a ${htmlService.bold(Constraints.GROUP_ONLY)} command.`
        )
      }

      return originalMethod.apply(this, args)
    }
    return descriptor
  }
}
