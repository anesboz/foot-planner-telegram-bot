import Joi from 'joi'
import TelegramBot from 'node-telegram-bot-api'
import { MError } from '../command.enums'
import htmlService from '../../../services/bot/application/html.service'

export function validateMessage(this: any) {
  return function (
    target: any,
    propertyKey: string,
    descriptor: PropertyDescriptor
  ) {
    const originalMethod = descriptor.value
    descriptor.value = function (...args: any[]) {
      const message = args[0] as TelegramBot.Message

      if (message.text == null) {
        throw new MError(`Message can not be ${htmlService.codeInline('null')}`)
      }
      if (message.from == null) {
        throw new MError(
          `message.from can not be ${htmlService.codeInline('null')}`
        )
      }
      // no html tag as input
      if (/[<>&]/.test(message.text) == null) {
        throw new MError(
          `message.text can not contain ${htmlService.codeInline('<>&')}`
        )
      }

      return originalMethod.apply(this, args)
    }
    return descriptor
  }
}
